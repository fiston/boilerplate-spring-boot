package rw.viden.boilerplate.filters

import org.springframework.core.Ordered.HIGHEST_PRECEDENCE
import org.springframework.core.annotation.Order
import org.springframework.http.server.reactive.ServerHttpRequest
import org.springframework.stereotype.Component
import org.springframework.web.server.ServerWebExchange
import org.springframework.web.server.WebFilter
import org.springframework.web.server.WebFilterChain
import reactor.core.publisher.Mono

@Component
@Order(HIGHEST_PRECEDENCE)
class BasePathFilter : WebFilter {
    private var basePath: String = "/boilerplate"

    override fun filter(exchange: ServerWebExchange, chain: WebFilterChain): Mono<Void> {
        val request: ServerHttpRequest = exchange.request
        println("=======request.uri.path: ${request.uri.path}")
        if (!request.uri.path.startsWith(basePath)) {
            val newPath = basePath + request.uri.path
            throw RuntimeException("Invalid path: $newPath")
//            val redirectRequest = request.mutate().path(newPath).build()
//            return chain.filter(exchange.mutate().request(redirectRequest).build())
        }

        return chain.filter(exchange)
    }
}
