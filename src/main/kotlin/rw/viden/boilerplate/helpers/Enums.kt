package rw.viden.boilerplate.helpers

enum class RequestStatus {
    PENDING, APPROVED, REJECTED
}

enum class DisbursementStatus {
    INITIATED,  DONE
}

enum class PaymentStatus {
    SCHEDULED,  OVERDUE, PAID
}