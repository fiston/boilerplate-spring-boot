package rw.viden.boilerplate.services

import io.swagger.v3.oas.annotations.Parameter
import io.swagger.v3.oas.annotations.enums.ParameterIn
import io.swagger.v3.oas.annotations.parameters.RequestBody
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.security.authentication.AccountExpiredException
import org.springframework.security.authentication.CredentialsExpiredException
import org.springframework.security.authentication.DisabledException
import org.springframework.security.authentication.LockedException
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Service
import reactor.core.publisher.Mono
import rw.viden.boilerplate.exceptions.*
import rw.viden.boilerplate.models.databases.CustomerProfile
import rw.viden.boilerplate.models.databases.Users
import rw.viden.boilerplate.models.requests.*
import rw.viden.boilerplate.models.responses.AuthenticationResponse
import rw.viden.boilerplate.models.responses.HttpDefaultResponse
import rw.viden.boilerplate.models.responses.RegistrationResponse
import rw.viden.boilerplate.security.JwtUtils
import rw.viden.boilerplate.utils.UserRoles
import rw.viden.boilerplate.utils.UserStatus
import rw.viden.boilerplate.utils.toPrettyJsonString
import rw.viden.boilerplate.utils.toRwandan12DigitPhone
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime
import java.util.*


interface AuthenticationService {

    fun login(
        authenticationRequest: AuthenticationRequest,
        sessionData: MutableMap<String, String>
    ): Mono<AuthenticationResponse>

    fun refreshToken(
        refreshToken: String,
        sessionData: MutableMap<String, String>
    ): Mono<AuthenticationResponse>

    fun logout(logoutRequest: LogoutRequest): Mono<HttpDefaultResponse>

    fun verifyPassword(
        verifyPasswordRequest: VerifyPasswordRequest,
        sessionData: MutableMap<String, String>
    ): Mono<HttpDefaultResponse>

    fun activateAccount(
        @Parameter(`in` = ParameterIn.PATH, name = "userId") userId: UUID,
        @Parameter(`in` = ParameterIn.PATH, name = "activationCode") activationCode: String
    ): Mono<HttpDefaultResponse>

    fun resendActivationCode(
        @Parameter(`in` = ParameterIn.PATH, name = "userId") userId: UUID
    ): Mono<HttpDefaultResponse>

    fun whoAmI(userToken: String): Mono<Users>

    fun signup(profileRequest: SelfOnboardingRequest): Mono<RegistrationResponse>

    fun voidUser(@Parameter(`in` = ParameterIn.PATH, name = "userId") userId: UUID): Mono<Users>

    fun confirmUser(@Parameter(`in` = ParameterIn.PATH, name = "id") userId: UUID): Mono<Users>

    fun updatePassword(
        @Parameter(`in` = ParameterIn.PATH, name = "id") userId: UUID,
        selfPasswordResetRequest: SelfPasswordResetRequest
    ): Mono<HttpDefaultResponse>

    fun completeUserProfile(
        @Parameter(`in` = ParameterIn.PATH, name = "userProfileId") userProfileId: UUID,
        @RequestBody userRequest: UsersProfileRequest
    ): Mono<RegistrationResponse>

    fun changePassword(passwordResetRequest: PasswordResetRequest): Mono<HttpDefaultResponse>
}

@Service
class AuthenticationServiceImpl(
    private var usersService: UsersService,
    private var customerProfileService: CustomerProfileService,
    private var bCryptPasswordEncoder: BCryptPasswordEncoder,
    private var jwtUtils: JwtUtils,
    private val emailService: EmailService,
    private val confirmationService: ConfirmationService
) : AuthenticationService {

    private val logger: Logger = LoggerFactory.getLogger(AuthenticationService::class.java)

    override fun signup(@RequestBody profileRequest: SelfOnboardingRequest): Mono<RegistrationResponse> {
        logger.info("Creating customer profile: $profileRequest")
        val phoneNumber = profileRequest.phoneNumber.toRwandan12DigitPhone()

        val phoneNumberOrEmailExistsMono =
            customerProfileService.existsByPhoneNumberOrEmail(phoneNumber, profileRequest.email.lowercase())
        val userNameExistsMono = usersService.existsByUsername(profileRequest.username.lowercase())

        return Mono.zip(phoneNumberOrEmailExistsMono, userNameExistsMono)
            .flatMap { tuple ->
                val phoneNumberOrEmailExists = tuple.t1
                val userNameExists = tuple.t2

                if (phoneNumberOrEmailExists) {
                    Mono.error(DuplicateViolation("PHONE_NUMBER_ALREADY_SAVED_CONFLICT"))
                } else if (userNameExists) {
                    // Either email or phone already exists, return an error response.
                    Mono.error(DuplicateViolation("USERNAME_ALREADY_SAVED_CONFLICT"))
                } else {
                    usersService.add(
                        Users(
                            username = profileRequest.username.lowercase(Locale.getDefault()),
                            password = bCryptPasswordEncoder.encode(profileRequest.password),
                            role = profileRequest.role,
                        ).setAsNew()
                    ).flatMap { user ->

                        val customerProfile = CustomerProfile(
                            firstName = profileRequest.firstName,
                            lastName = profileRequest.lastName,
                            email = profileRequest.email,
                            phoneNumber = profileRequest.phoneNumber,
                            legalDocument = null,
                            legalDocumentType = null,
                            currentAddress = null,
                            permanentAddress = null,
                            contactNumber = null,
                            dateOfBirth = null,
                            kycComplete = false,
                            job = null,
                            employer = null,
                            createdDate = LocalDateTime.now(),
                            userId = user.id!!
                        ).setAsNew()

                        customerProfileService.add(customerProfile).flatMap {
                            generateAndShareConfirmationCode(it)
                        }

                    }
                }
            }.onErrorMap {
                it.printStackTrace()
                BadRequestException("COULD_NOT_SAVE_CUSTOMER_${it.message}")
            }
    }

    private fun generateAndShareConfirmationCode(
        customerProfile: CustomerProfile
    ): Mono<RegistrationResponse> {
        return confirmationService.generateNewOrGetConfirmationCode(customerProfile.userId, customerProfile.email).map {
            emailService.sendEmail(
                customerProfile.email,
                "Welcome to Boilerplate",
                "Welcome to Boilerplate,\n Confirmation code is $it"
            )
        }.then(
            Mono.just(
                RegistrationResponse(
                    "USER_CREATED",
                    UserStatus.PENDING_APPROVAL,
                    customerProfile.userId.toString()
                )
            )
        )

    }


    override fun voidUser(userId: UUID): Mono<Users> {

        val user = usersService.getOne(userId)
        return user.flatMap {
            usersService.updateUsers(it.id!!, it.apply { status = UserStatus.DELETED })
        }.switchIfEmpty(Mono.error(NotFoundException("USER_NOT_FOUND")))
    }

    override fun confirmUser(userId: UUID): Mono<Users> {
        val user = usersService.getOne(userId)
        return user.flatMap {
            usersService.updateUsers(userId, it.apply {
                status = UserStatus.ACTIVE
                enabled = true
            })
        }.switchIfEmpty(Mono.error(NotFoundException("USER_NOT_FOUND")))
    }

    override fun updatePassword(
        userId: UUID,
        selfPasswordResetRequest: SelfPasswordResetRequest
    ): Mono<HttpDefaultResponse> {
        return usersService.getOne(userId)
            .switchIfEmpty(Mono.error(NotFoundException("USER_NOT_FOUND")))
            .flatMap { user ->
                checkPassword(user.password, selfPasswordResetRequest.oldPassword)
                usersService.updateUsers(
                    userId,
                    user.apply {
                        password = bCryptPasswordEncoder.encode(selfPasswordResetRequest.newPassword)
                        isResettingPassword = false
                    })
                    .flatMap {
                        Mono.just(
                            HttpDefaultResponse(
                                status = "200",
                                message = "PASSWORD_UPDATED",
                                LocalTime.now().toString()
                            )
                        )
                    }

            }
    }

    override fun completeUserProfile(
        userProfileId: UUID,
        userRequest: UsersProfileRequest
    ): Mono<RegistrationResponse> {
        val phoneNumber = userRequest.phoneNumber.toRwandan12DigitPhone()

        val userProfileMono =
            customerProfileService.getOne(userProfileId)
                .switchIfEmpty(Mono.error(NotFoundException("PROFILE_NOT_FOUND")))

        return userProfileMono
            .flatMap { userProfile ->
                usersService.getOne(userProfile.userId).flatMap { user ->
                    val userToSave = user.copy(
                        username = userRequest.username.lowercase(),
                        password = user.password,
                        role = UserRoles.USER,
                    )

                    usersService.updateUsers(userProfile.userId, userToSave).flatMap { usr ->
                        val usersProfile: CustomerProfile? = usr.id?.let {
                            userProfile.copy(
                                firstName = userRequest.firstName,
                                lastName = userRequest.lastName,
                                email = userRequest.email,
                                phoneNumber = phoneNumber,
                                userId = it,
                                legalDocument = userRequest.legalDocument,
                                legalDocumentType = userRequest.legalDocumentType,
                                currentAddress = userRequest.currentAddress,
                                permanentAddress = userRequest.permanentAddress,
                                contactNumber = userRequest.contactNumber,
                                dateOfBirth = LocalDate.parse(userRequest.dateOfBirth),
                                kycComplete = false,
                                job = userRequest.job,
                                employer = userRequest.employer,
                            ).setIsKycComplete()

                        }
                        customerProfileService.add(usersProfile!!).map {
                            RegistrationResponse("KYC_UPDATED", usr.status, usr.id.toString())
                        }
                    }
                }
            }.onErrorMap { throw InternalServerErrorException(it.message ?: it.localizedMessage) }
    }

    override fun changePassword(passwordResetRequest: PasswordResetRequest): Mono<HttpDefaultResponse> {
        return usersService.findByUsername(passwordResetRequest.username)
            .switchIfEmpty(Mono.error(NotFoundException("USER_NOT_FOUND")))
            .flatMap { user ->
                usersService.updateUsers(
                    user.id!!,
                    user.apply {
                        password = bCryptPasswordEncoder.encode(passwordResetRequest.newPassword)
                        isResettingPassword = true
                    })
                    .flatMap {
                        Mono.just(
                            HttpDefaultResponse(
                                status = "200",
                                message = "PASSWORD_UPDATED",
                                LocalTime.now().toString()
                            )
                        )
                    }

            }
    }

    override fun login(
        authenticationRequest: AuthenticationRequest,
        sessionData: MutableMap<String, String>
    ): Mono<AuthenticationResponse> {
        val fallback: Mono<Users> =
            Mono.error(UnauthorizedException("No user account was found with username ${authenticationRequest.username.lowercase()}"))

        val user = usersService.findByUsername(authenticationRequest.username.lowercase()).switchIfEmpty(fallback)

        return user.map { usr ->
            logger.info("Login find user")
            checkPassword(usr.password, authenticationRequest.password)
            check(usr)
            val token = jwtUtils.generateSessionToken(usr, sessionData)

            if (!usr.isResettingPassword)
                AuthenticationResponse(
                    token = token,
                    refreshToken = token,
                    isResettingPassword = usr.isResettingPassword,
                    userId = usr.id!!

                )
            else
                AuthenticationResponse(
                    token = "",
                    refreshToken = "",
                    isResettingPassword = usr.isResettingPassword,
                    userId = usr.id!!

                )

        }.log()
            .doOnNext {
                if (it == null)
                    throw UnauthorizedException("BAD_CREDENTIAL")
            }
            .onErrorMap {
                logger.error("\n++++++++++++ ${it.message}\n==========")

                throw UnauthorizedException(it.message ?: it.localizedMessage)
            }

    }

    override fun refreshToken(
        refreshToken: String,
        sessionData: MutableMap<String, String>
    ): Mono<AuthenticationResponse> {
        if (!jwtUtils.validateRefreshToken(refreshToken)) {
            throw UnauthorizedException("BAD_TOKEN")
        }

        val username = jwtUtils.getUserNameFromToken(refreshToken).lowercase()
        val userMono = usersService.findByUsername(username)
            .switchIfEmpty(Mono.error(UnauthorizedException("No user account was found with username $username")))

        return userMono.flatMap { user ->
            logger.info("User found: ${user.username}")

            check(user)

            val token = jwtUtils.generateSessionToken(user, sessionData)
            logger.info("New token generated: $token")

            Mono.just(
                AuthenticationResponse(
                    token = token,
                    refreshToken = token,
                    isResettingPassword = user.isResettingPassword,
                    userId = user.id!!
                )
            )
        }
            .doOnNext { authenticationResponse ->
                if (authenticationResponse == null) {
                    throw UnauthorizedException("BAD_CREDENTIAL")
                }
            }
            .onErrorMap { throwable ->
                logger.error("An error occurred during token refresh: ${throwable.message}")
                throwable.printStackTrace()
                UnauthorizedException("BAD_TOKEN")
            }
    }


    override fun logout(logoutRequest: LogoutRequest): Mono<HttpDefaultResponse> {
        val (accessToken, refreshToken) = logoutRequest
        val isSuccess = jwtUtils.logout(accessToken, refreshToken)

        val status = if (isSuccess) "200" else "400"
        val message = if (isSuccess) "LOGOUT_SUCCESSFUL" else "LOGOUT_FAILED"

        return Mono.just(HttpDefaultResponse(status, message, LocalTime.now().toString()))
    }

    override fun verifyPassword(
        verifyPasswordRequest: VerifyPasswordRequest,
        sessionData: MutableMap<String, String>
    ): Mono<HttpDefaultResponse> {
        val validateToken = jwtUtils.validatingToken(verifyPasswordRequest.accessToken, sessionData)
        val userMono = usersService.findByUsername(verifyPasswordRequest.username)
            .switchIfEmpty(Mono.error(UnauthorizedException("No user account was found with username ${verifyPasswordRequest.username}")))
        return if (validateToken)
            userMono.map { usr ->
                logger.info("Login find user")
                checkPassword(usr.password, verifyPasswordRequest.password)
                HttpDefaultResponse(status = "200", message = "PASSOWRD_CORRECT", LocalTime.now().toString())
            }
        else
            Mono.error(UnauthorizedException("BAD_TOKEN"))
    }

    override fun activateAccount(userId: UUID, activationCode: String): Mono<HttpDefaultResponse> {
        return customerProfileService.findByUserId(userId)
            .switchIfEmpty(Mono.error(NotFoundException("USER_NOT_FOUND")))
            .flatMap { customerProfile ->
                confirmationService.verifyConfirmationCode(userId, customerProfile.email, activationCode)
            }
            .filter { it }
            .switchIfEmpty(Mono.error(NotFoundException("INVALID_ACTIVATION_CODE")))
            .flatMap {
                usersService.findByIdAndStatus(userId, UserStatus.PENDING_APPROVAL)
                    .flatMap { user ->
                        usersService.updateUsers(userId, user.apply {
                            enabled = true
                            status = UserStatus.ACTIVE
                        })
                            .map {
                                HttpDefaultResponse(
                                    status = "200",
                                    message = "ACCOUNT_ACTIVATED",
                                    LocalTime.now().toString()
                                )
                            }
                    }.switchIfEmpty(Mono.error(NotFoundException("USER_NOT_FOUND_OR_ALREADY_CONFIRMED")))
            }.onErrorResume { error ->
                logger.error(error.localizedMessage)
                Mono.just(
                    HttpDefaultResponse(
                        status = "400",
                        message = error.localizedMessage,
                        LocalTime.now().toString()
                    )
                )
            }
    }

    override fun resendActivationCode(userId: UUID): Mono<HttpDefaultResponse> {
        return usersService.existByIdAndStatus(userId, UserStatus.PENDING_APPROVAL)
            .switchIfEmpty(Mono.error(ForbiddenException("USER_NOT_FOUND_OR_ALREADY_CONFIRMED")))
            .flatMap {userExist->
                if (!userExist)
                    Mono.error(ForbiddenException("USER_NOT_FOUND_OR_ALREADY_CONFIRMED"))
                else
                    customerProfileService.findByUserId(userId).flatMap {customerProfile->
                        generateAndShareConfirmationCode(customerProfile)
                    }.map {
                        HttpDefaultResponse(
                            status = "200",
                            message = "ACTIVATION_CODE_RESENT",
                            LocalTime.now().toString()
                        )
                    }.onErrorResume { error ->
                        logger.error(error.localizedMessage)
                        Mono.just(
                            HttpDefaultResponse(
                                status = "400",
                                message = error.localizedMessage,
                                LocalTime.now().toString()
                            )
                        )
                    }
            }
    }

    override fun whoAmI(userToken: String): Mono<Users> {
        logger.info(
            "\n\n\n ========\n ${
                HttpDefaultResponse(
                    status = "200",
                    message = "Activation code sent",
                    timestamps = LocalTime.now().toString()
                ).toPrettyJsonString()
            } \n========\n\n\n"
        )
        val token = userToken.replace("Bearer ", "")
        if (jwtUtils.validateJwtToken(token)) {
            logger.info("Token is valid, extracting username")
            val username = jwtUtils.getUserNameFromToken(token)
            logger.info("Username extracted: $username")
            return usersService.findByUsername(username)
                .switchIfEmpty(Mono.error(NotFoundException("USER_NOT_FOUND")))
        } else throw InvalidJwtTokenException("INVALID_TOKEN")
    }

    private fun check(user: Users) {

        when {
            !user.isAccountNonLocked -> throw LockedException("ACCOUNT_LOCKED")

            !user.isEnabled -> throw DisabledException("USER_DISABLED")

            !user.isAccountNonExpired -> throw AccountExpiredException("ACCOUNT_EXPIRED")

            !user.isCredentialsNonExpired -> throw CredentialsExpiredException("CREDENTIALS_EXPIRED")

            user.status == UserStatus.DELETED -> throw AccountStatusException("ACCOUNT_DELETED")

            user.status == UserStatus.PENDING_APPROVAL -> throw AccountStatusException("PENDING_APPROVAL")
        }
    }

    fun checkPassword(encryptedPassword: String, unencryptedPassword: String) {
        if (!bCryptPasswordEncoder.matches(unencryptedPassword, encryptedPassword))
            throw UnauthorizedException("BAD_CREDENTIALS")
    }


}

