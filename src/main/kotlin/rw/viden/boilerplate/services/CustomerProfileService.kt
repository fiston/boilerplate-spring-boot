package rw.viden.boilerplate.services

import org.springframework.stereotype.Service
import reactor.core.publisher.Mono
import rw.viden.boilerplate.models.databases.CustomerProfile
import rw.viden.boilerplate.repository.CustomerProfileRepository
import java.util.*

@Service
class CustomerProfileService(override var repository: CustomerProfileRepository) :
    GenericService<CustomerProfile, CustomerProfileRepository>() {

    fun findByUserId(userId: UUID): Mono<CustomerProfile> {
        return repository.findByUserId(userId)
    }

    fun existsByEmail(email: String): Mono<Boolean> {
        return repository.existsByEmail(email)
    }

    fun existsByPhoneNumber(phoneNumber: String): Mono<Boolean> {
        return repository.existsByPhoneNumber(phoneNumber)
    }

    fun existsByPhoneNumberOrEmail(phoneNumber: String, email: String): Mono<Boolean> {
        return repository.existsByPhoneNumberOrEmail(phoneNumber, email)
    }

    fun updateProfile(userProfileId: UUID, customerProfile: CustomerProfile): Mono<CustomerProfile> {
        return repository.findById(userProfileId)
            .flatMap { existingProfile ->
                existingProfile.firstName = customerProfile.firstName
                existingProfile.lastName = customerProfile.lastName
                existingProfile.phoneNumber = customerProfile.phoneNumber
                existingProfile.email = customerProfile.email
                repository.save(existingProfile)
            }
    }
}