package rw.viden.boilerplate.services

import org.springframework.stereotype.Service
import reactor.core.publisher.Mono
import rw.viden.boilerplate.models.databases.Users
import rw.viden.boilerplate.repository.UsersRepository
import rw.viden.boilerplate.utils.UserStatus
import java.util.*


@Service
class UsersService(override var repository: UsersRepository) : GenericService<Users, UsersRepository>() {

    fun findByUsername(username: String): Mono<Users> {
        return repository.findByUsername(username)
    }

    fun findByIdAndStatus(id: UUID, status: UserStatus): Mono<Users> {
        return repository.findByIdAndStatus(id, status)
    }

    fun existByIdAndStatus(id: UUID, status: UserStatus): Mono<Boolean> {
        return repository.existsByIdAndStatus(id, status)
    }

    fun existsByUsername(username: String): Mono<Boolean> {
        return repository.existsByUsername(username)
    }

    fun updateUsers(id: UUID, users: Users): Mono<Users> {
        return repository.findById(id)
            .flatMap {
                val updatedUsers: Users = users
                    .apply { this.id = id }
                repository.save(updatedUsers)
            }.switchIfEmpty(this.add(users))
    }
}