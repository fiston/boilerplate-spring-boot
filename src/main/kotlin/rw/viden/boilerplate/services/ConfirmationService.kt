package rw.viden.boilerplate.services

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import reactor.core.publisher.Mono
import rw.viden.boilerplate.models.databases.ConfirmationCode
import rw.viden.boilerplate.repository.ConfirmationCodeRepository
import java.time.LocalDateTime
import java.util.*

@Service
class ConfirmationService(private var confirmationCodeRepository: ConfirmationCodeRepository) {

    val logger:Logger=LoggerFactory.getLogger(this::class.java)

    fun generateConfirmationCode(userId: UUID, email: String): Mono<String> {
        logger.info("Generating confirmation code for user with email: $email")
        val code = generateRandomCode()
        val expiryTime = LocalDateTime.now().plusHours(12)
        return confirmationCodeRepository.save(
            ConfirmationCode(
                userId = userId,
                emailPhoneNumber = email,
                token = code,
                expiryDateTime = expiryTime
            ).setAsNew()
        ).map {
            code
        }

    }
    fun generateNewOrGetConfirmationCode(userId: UUID, email: String): Mono<String> {
        logger.info("Generating confirmation code for user with email: $email")
        // Check if there's an existing code and if it has not expired
        return confirmationCodeRepository.findByUserIdAndExpiryDateTimeIsAfter(userId, LocalDateTime.now())
            .flatMap { existingCode ->
                    Mono.just(existingCode.token)

            }.switchIfEmpty (generateConfirmationCode(userId, email))
    }


    fun verifyConfirmationCode(userId: UUID, email: String, code: String): Mono<Boolean> {
      logger.info("Check if the code exists in the database and is not expired")
        return confirmationCodeRepository.findByEmailPhoneNumberAndTokenAndExpiryDateTimeIsAfter(email, code, LocalDateTime.now())
            // If confirmationCode is not null, mark email as confirmed and delete the confirmation code
            .flatMap { confirmationCode ->
                confirmationCodeRepository.delete(confirmationCode).then(Mono.just(confirmationCode.userId == userId))

            }.switchIfEmpty(Mono.just(false))
    }

    private fun generateRandomCode(): String {
        // Generate a random confirmation code
        return UUID.randomUUID().toString().substring(0, 6).uppercase()
    }
}