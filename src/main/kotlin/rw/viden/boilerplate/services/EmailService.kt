package rw.viden.boilerplate.services

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.mail.SimpleMailMessage
import org.springframework.mail.javamail.JavaMailSender
import org.springframework.stereotype.Service
import rw.viden.boilerplate.configurations.ApplicationProperties

@Service
class EmailService(private val mailSender: JavaMailSender, private val applicationProperties: ApplicationProperties) {

    val logger: Logger = LoggerFactory.getLogger(this::class.java)

    fun sendEmail(to: String, subject: String, body: String) {
        val message = SimpleMailMessage()
        message.setTo(to.trim())
        message.from = applicationProperties.mailUsername
        message.subject = subject
        message.text = body

        mailSender.send(message)
    }
}
