package rw.viden.boilerplate.routes.swagger

import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.Parameter
import io.swagger.v3.oas.annotations.enums.ParameterIn
import io.swagger.v3.oas.annotations.media.ArraySchema
import io.swagger.v3.oas.annotations.media.Content
import io.swagger.v3.oas.annotations.media.ExampleObject
import io.swagger.v3.oas.annotations.media.Schema
import io.swagger.v3.oas.annotations.parameters.RequestBody
import io.swagger.v3.oas.annotations.responses.ApiResponse
import org.springdoc.core.annotations.RouterOperation
import org.springdoc.core.annotations.RouterOperations
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.RequestMethod
import rw.viden.boilerplate.exceptions.GlobalExceptionHandler
import rw.viden.boilerplate.models.databases.Users
import rw.viden.boilerplate.models.requests.*
import rw.viden.boilerplate.models.responses.AuthenticationResponse
import rw.viden.boilerplate.models.responses.HttpDefaultResponse
import rw.viden.boilerplate.models.responses.RegistrationResponse
import rw.viden.boilerplate.models.responses.UsersResponse


@Retention(AnnotationRetention.RUNTIME)
@Target(
    AnnotationTarget.FUNCTION,
    AnnotationTarget.PROPERTY_GETTER,
    AnnotationTarget.PROPERTY_SETTER,
    AnnotationTarget.ANNOTATION_CLASS,
    AnnotationTarget.CLASS
)
@RouterOperations(
    RouterOperation(
        method = arrayOf(RequestMethod.POST),
        path = "/login",
        operation = Operation(
            description = "logging in",
            requestBody = RequestBody(
                description = "logging in",
                required = true,
                content = [
                    Content(
                        schema = Schema(implementation = AuthenticationRequest::class),
                        examples = [ExampleObject()]
                    )
                ]

            ),
            operationId = "login",
            tags = arrayOf("USERS"),
            responses = [ApiResponse(
                responseCode = "200",
                description = "Authentication Response",
                content = [Content(
                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = Schema(implementation = AuthenticationResponse::class)
                )]
            ), ApiResponse(
                responseCode = "400",
                description = "Bad Request response",
                content = [Content(
                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = Schema(implementation = GlobalExceptionHandler.HttpError::class),
                    examples = [ExampleObject()]
                )]
            ), ApiResponse(
                responseCode = "404",
                description = "Not found response",
                content = [Content(
                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = Schema(implementation = GlobalExceptionHandler.HttpError::class),
                    examples = [ExampleObject()]
                )]
            )]
        )
    ),
    RouterOperation(
        method = arrayOf(RequestMethod.GET),
        path = "/who",
        operation = Operation(
            description = "who am i",
            operationId = "whoami",
            tags = arrayOf("USERS"),
            responses = [ApiResponse(
                responseCode = "200",
                description = "Users Response",
                content = [Content(
                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = Schema(implementation = UsersResponse::class), examples = [ExampleObject()]
                )]
            ), ApiResponse(
                responseCode = "400",
                description = "Bad Request response",
                content = [Content(
                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = Schema(implementation = GlobalExceptionHandler.HttpError::class),
                    examples = [ExampleObject()]
                )]
            ), ApiResponse(
                responseCode = "404",
                description = "Not found response",
                content = [Content(
                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = Schema(implementation = GlobalExceptionHandler.HttpError::class),
                    examples = [ExampleObject()]
                )]
            )]
        )
    ),
    RouterOperation(
        method = arrayOf(RequestMethod.GET),
        path = "/users",
        operation = Operation(
            description = "Get all Users",
            operationId = "listAllUsers",
            tags = arrayOf("USERS"),
            responses = [ApiResponse(
                responseCode = "200",
                description = "Users Response",
                content = [Content(
                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                    array = ArraySchema(schema = Schema(implementation = UsersResponse::class)),
                    examples = [ExampleObject()]
                )]
            ), ApiResponse(
                responseCode = "400",
                description = "Bad Request response",
                content = [Content(
                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = Schema(implementation = GlobalExceptionHandler.HttpError::class),
                    examples = [ExampleObject()]
                )]
            ), ApiResponse(
                responseCode = "404",
                description = "Not found response",
                content = [Content(
                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = Schema(implementation = GlobalExceptionHandler.HttpError::class),
                    examples = [ExampleObject()]
                )]
            )]
        )
    ),
    RouterOperation(
        method = arrayOf(RequestMethod.POST),
        path = "/update-customer/{userProfileId}",
        operation = Operation(
            description = "Update Customer",
            requestBody = RequestBody(
                description = "Updating Customer Profile",
                required = true,
                content = [
                    Content(
                        schema = Schema(implementation = UsersProfileRequest::class),
                        examples = [ExampleObject()]
                    )

                ]

            ),
            operationId = "completeProfileData",
            tags = arrayOf("USERS"),
            responses = [ApiResponse(
                responseCode = "200",
                description = "Create Customer Response",
                content = [Content(
                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = Schema(implementation = RegistrationResponse::class),
                    examples = [ExampleObject()]
                )]
            ), ApiResponse(
                responseCode = "400",
                description = "Bad Request response",
                content = [Content(
                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = Schema(implementation = GlobalExceptionHandler.HttpError::class),
                    examples = [ExampleObject()]
                )]
            ), ApiResponse(
                responseCode = "404",
                description = "Not found response",
                content = [Content(
                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = Schema(implementation = GlobalExceptionHandler.HttpError::class),
                    examples = [ExampleObject()]
                )]
            )]
        )
    ),
    RouterOperation(
        method = arrayOf(RequestMethod.GET),
        path = "/users/{id}/confirm",
        operation = Operation(
            description = "Confirm Users",
            parameters = [Parameter(
                name = "id",
                description = "UserID to confirm",
                `in` = ParameterIn.PATH,
                required = true
            )],
            operationId = "confirmUser",
            tags = arrayOf("USERS"),
            responses = [ApiResponse(
                responseCode = "200",
                description = "Confirm Response",
                content = [Content(
                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = Schema(implementation = Users::class),
                    examples = [ExampleObject()]
                )]
            ), ApiResponse(
                responseCode = "400",
                description = "Bad Request response",
                content = [Content(
                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = Schema(implementation = GlobalExceptionHandler.HttpError::class),
                    examples = [ExampleObject()]
                )]
            ), ApiResponse(
                responseCode = "404",
                description = "Not found response",
                content = [Content(
                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = Schema(implementation = GlobalExceptionHandler.HttpError::class),
                    examples = [ExampleObject()]
                )]
            )]
        )
    ),
    RouterOperation(
        method = arrayOf(RequestMethod.DELETE),
        path = "/deactivate-user/{userId}",
        operation = Operation(
            description = "voidUser",
            parameters = [Parameter(
                name = "userId",
                description = "UserID to DELETE",
                `in` = ParameterIn.PATH,
                required = true
            )],
            operationId = "voidUser",
            tags = arrayOf("USERS"),
            responses = [ApiResponse(
                responseCode = "200",
                description = "Voided User Response",
                content = [Content(
                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = Schema(implementation = Users::class),
                    examples = [ExampleObject()]

                )]
            ), ApiResponse(
                responseCode = "400",
                description = "Bad Request response",
                content = [Content(
                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = Schema(implementation = GlobalExceptionHandler.HttpError::class),
                    examples = [ExampleObject()]
                )]
            ), ApiResponse(
                responseCode = "404",
                description = "Not found response",
                content = [Content(
                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = Schema(implementation = GlobalExceptionHandler.HttpError::class),
                    examples = [ExampleObject()]
                )]
            )]
        )
    ),
    RouterOperation(
        method = arrayOf(RequestMethod.PATCH),
        path = "/activate-user/{userId}/{activationCode}",
        operation = Operation(
            description = "activateAccount",
            parameters = [Parameter(
                name = "userId",
                description = "userId to activate",
                `in` = ParameterIn.PATH,
                required = true
            ),
                Parameter(
                    name = "activationCode",
                    description = "activationCode for activating userId",
                    `in` = ParameterIn.PATH,
                    required = true
                )],
            operationId = "activateAccount",
            tags = arrayOf("USERS"),
            responses = [ApiResponse(
                responseCode = "200",
                description = "Activated User Response",
                content = [Content(
                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = Schema(implementation = HttpDefaultResponse::class),
                    examples = [ExampleObject()]

                )]
            ), ApiResponse(
                responseCode = "400",
                description = "Bad Request response",
                content = [Content(
                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = Schema(implementation = GlobalExceptionHandler.HttpError::class),
                    examples = [ExampleObject()]
                )]
            ), ApiResponse(
                responseCode = "404",
                description = "Not found response",
                content = [Content(
                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = Schema(implementation = GlobalExceptionHandler.HttpError::class),
                    examples = [ExampleObject()]
                )]
            )]
        )
    ),
    RouterOperation(
        method = arrayOf(RequestMethod.GET),
        path = "/re-send-activation-code/{userId}",
        operation = Operation(
            description = "resendActivationCode",
            parameters = [Parameter(
                name = "userId",
                description = "userId to resend activation code",
                `in` = ParameterIn.PATH,
                required = true
            )],
            operationId = "resendActivationCode",
            tags = arrayOf("USERS"),
            responses = [ApiResponse(
                responseCode = "200",
                description = "Resend Activation Code Response",
                content = [Content(
                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = Schema(implementation = HttpDefaultResponse::class),
                    examples = [ExampleObject(
                        "{\n" +
                                "  \"status\" : \"200\",\n" +
                                "  \"message\" : \"ACTIVATION_CODE_SENT\",\n" +
                                "  \"timestamps\" : \"13:46:13.154739\"\n" +
                                "} "
                    )]

                )]
            ), ApiResponse(
                responseCode = "400",
                description = "Bad Request response",
                content = [Content(
                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = Schema(implementation = GlobalExceptionHandler.HttpError::class),
                    examples = [ExampleObject()]
                )]
            ), ApiResponse(
                responseCode = "404",
                description = "Not found response",
                content = [Content(
                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = Schema(implementation = GlobalExceptionHandler.HttpError::class),
                    examples = [ExampleObject()]
                )]
            )]
        )
    ),
    RouterOperation(
        method = arrayOf(RequestMethod.POST),
        path = "/verify-password",
        operation = Operation(
            description = "verifyPassword",
            requestBody = RequestBody(
                description = "Verify Password",
                required = true,
                content = [
                    Content(
                        schema = Schema(implementation = VerifyPasswordRequest::class),
                        examples = [ExampleObject()]
                    )

                ]

            ),
            operationId = "verifyPassword",
            tags = arrayOf("USERS"),
            responses = [ApiResponse(
                responseCode = "200",
                description = "Verify Password Response",
                content = [Content(
                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = Schema(implementation = Users::class),
                    examples = [ExampleObject()]

                )]
            ), ApiResponse(
                responseCode = "400",
                description = "Bad Request response",
                content = [Content(
                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = Schema(implementation = GlobalExceptionHandler.HttpError::class),
                    examples = [ExampleObject()]
                )]
            ), ApiResponse(
                responseCode = "404",
                description = "Not found response",
                content = [Content(
                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = Schema(implementation = GlobalExceptionHandler.HttpError::class),
                    examples = [ExampleObject()]
                )]
            )]
        )
    ),
    RouterOperation(
        method = arrayOf(RequestMethod.PATCH),
        path = "/users/{id}",
        operation = Operation(
            description = "selfUpdatePassword",
            parameters = [Parameter(
                name = "id",
                description = "UserID to update",
                `in` = ParameterIn.PATH,
                required = true
            )],
            requestBody = RequestBody(
                description = "logging in",
                required = true,
                content = [
                    Content(
                        schema = Schema(implementation = SelfPasswordResetRequest::class),
                        examples = [ExampleObject()]
                    )

                ]

            ),
            operationId = "selfUpdatePassword",
            tags = arrayOf("USERS"),
            responses = [ApiResponse(
                responseCode = "200",
                description = "Updated User Response",
                content = [Content(
                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = Schema(implementation = HttpDefaultResponse::class),
                    examples = [ExampleObject()]

                )]
            ), ApiResponse(
                responseCode = "400",
                description = "Bad Request response",
                content = [Content(
                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = Schema(implementation = GlobalExceptionHandler.HttpError::class),
                    examples = [ExampleObject()]
                )]
            ), ApiResponse(
                responseCode = "404",
                description = "Not found response",
                content = [Content(
                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = Schema(implementation = GlobalExceptionHandler.HttpError::class),
                    examples = [ExampleObject()]
                )]
            )]
        )
    ),
    RouterOperation(
        method = arrayOf(RequestMethod.PATCH),
        path = "/users",
        operation = Operation(
            description = "changePassword",
            requestBody = RequestBody(
                description = "logging in",
                required = true,
                content = [
                    Content(
                        schema = Schema(implementation = PasswordResetRequest::class),
                        examples = [ExampleObject()]
                    )

                ]
            ),
            operationId = "changePassword",
            tags = arrayOf("USERS"),
            responses = [ApiResponse(
                responseCode = "200",
                description = "Updated User Response",
                content = [Content(
                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = Schema(implementation = HttpDefaultResponse::class),
                    examples = [ExampleObject()]

                )]
            ), ApiResponse(
                responseCode = "400",
                description = "Bad Request response",
                content = [Content(
                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = Schema(implementation = GlobalExceptionHandler.HttpError::class),
                    examples = [ExampleObject()]
                )]
            ), ApiResponse(
                responseCode = "404",
                description = "Not found response",
                content = [Content(
                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = Schema(implementation = GlobalExceptionHandler.HttpError::class),
                    examples = [ExampleObject()]
                )]
            )]
        )
    ),
    RouterOperation(
        method = arrayOf(RequestMethod.POST),
        path = "/signup",
        operation = Operation(
            description = "Signing Up",
            requestBody = RequestBody(
                description = "Creating new  User",
                required = true,
                content = [
                    Content(
                        schema = Schema(implementation = SelfOnboardingRequest::class),
                        examples = [ExampleObject()]
                    )

                ]

            ),
            operationId = "createUser",
            tags = arrayOf("USERS"),
            responses = [ApiResponse(
                responseCode = "200",
                description = "Signup Response",
                content = [Content(
                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = Schema(implementation = RegistrationResponse::class),
                    examples = [ExampleObject()]
                )]
            ), ApiResponse(
                responseCode = "400",
                description = "Bad Request response",
                content = [Content(
                    mediaType = MediaType.APPLICATION_JSON_VALUE,
                    schema = Schema(implementation = GlobalExceptionHandler.HttpError::class),
                    examples = [ExampleObject()]

                )]
            ), ApiResponse(
                responseCode = "404",
                description = "Not found response",
                content = [Content(
                    mediaType = MediaType.APPLICATION_JSON_VALUE, schema = Schema(
                        implementation = GlobalExceptionHandler.HttpError::class
                    ), examples = [ExampleObject()]
                )]
            )]
        )
    ),
)
annotation class AuthenticationApiInfo 