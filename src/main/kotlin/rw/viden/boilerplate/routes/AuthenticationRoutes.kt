package rw.viden.boilerplate.routes


import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Primary
import org.springframework.context.annotation.Profile
import org.springframework.http.MediaType
import org.springframework.web.reactive.function.server.RequestPredicates.*
import org.springframework.web.reactive.function.server.RouterFunction
import org.springframework.web.reactive.function.server.RouterFunctions
import org.springframework.web.reactive.function.server.RouterFunctions.route
import org.springframework.web.reactive.function.server.ServerResponse
import rw.viden.boilerplate.routes.handlers.AuthorizationHandler
import rw.viden.boilerplate.routes.swagger.AuthenticationApiInfo
import java.net.URI


@Configuration(proxyBeanMethods = false)
class AuthenticationRoutes {

    @Bean
    @AuthenticationApiInfo
    fun authenticationRoute(authorizationHandler: AuthorizationHandler): RouterFunction<ServerResponse> {
        return route(
            POST("/login").and(contentType(MediaType.APPLICATION_JSON).and(accept(MediaType.APPLICATION_JSON))),
            authorizationHandler::login
        )
            .andRoute(
                GET("/who").and(accept(MediaType.APPLICATION_JSON)),
                authorizationHandler::whoAmI
            )
            .andRoute(
                POST("/refresh-token").and(contentType(MediaType.APPLICATION_JSON).and(accept(MediaType.APPLICATION_JSON))),
                authorizationHandler::refreshToken
            )
            .andRoute(
                POST("/logout").and(contentType(MediaType.APPLICATION_JSON).and(accept(MediaType.APPLICATION_JSON))),
                authorizationHandler::logout
            )
            .andRoute(
                POST("/verify-password").and(contentType(MediaType.APPLICATION_JSON).and(accept(MediaType.APPLICATION_JSON))),
                authorizationHandler::verifyPassword
            )
            .andRoute(
                PATCH("/activate-user/{userId}/{activationCode}").and(contentType(MediaType.APPLICATION_JSON).and(accept(MediaType.APPLICATION_JSON))),
                authorizationHandler::activateAccount
            )
            .andRoute(
                GET("/re-send-activation-code/{userId}").and(contentType(MediaType.APPLICATION_JSON).and(accept(MediaType.APPLICATION_JSON))),
                authorizationHandler::resendActivationCode
            )
            .andRoute(
                POST("/update-customer/{userProfileId}").and(contentType(MediaType.APPLICATION_JSON).and(accept(MediaType.APPLICATION_JSON))),
                authorizationHandler::completeProfileData
            )
            .andRoute(
                POST("/signup").and(contentType(MediaType.APPLICATION_JSON).and(accept(MediaType.APPLICATION_JSON))),
                authorizationHandler::signup
            )
            .andRoute(
                DELETE("/deactivate-user/{userId}").and(contentType(MediaType.APPLICATION_JSON).and(accept(MediaType.APPLICATION_JSON))),
                authorizationHandler::voidUser
            )
            .andRoute(
                GET("/users/{id}/confirm").and(accept(MediaType.APPLICATION_JSON)),
                authorizationHandler::confirmUser
            )
            .andRoute(PATCH("/users/reset-password/{id}").and(contentType(MediaType.APPLICATION_JSON).and(accept(MediaType.APPLICATION_JSON))),
                authorizationHandler::selfUpdatePassword
            )
    }

    @Profile("dev")
    @Bean
    @Primary
    fun devRouter(authenticationRoute: RouterFunction<ServerResponse>) = authenticationRoute.filter { request, next ->
        println("Request path: ${request.path()}")
        // Conditionally add routes only for "dev" profile
        if (request.path() == "/") {
            ServerResponse.temporaryRedirect(URI.create("/boilerplate/swagger-ui.html")).build()
        } else {
            next.handle(request)
        }
    }
}