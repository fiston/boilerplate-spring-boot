package rw.viden.boilerplate.repository


import org.springframework.data.r2dbc.repository.R2dbcRepository
import org.springframework.stereotype.Repository
import reactor.core.publisher.Mono
import rw.viden.boilerplate.models.databases.ConfirmationCode
import java.time.LocalDateTime
import java.util.*

@Repository
interface ConfirmationCodeRepository:R2dbcRepository<ConfirmationCode, UUID> {

     fun findByEmailPhoneNumberAndTokenAndExpiryDateTimeIsAfter(email: String, code: String, now: LocalDateTime): Mono<ConfirmationCode>

     fun findByUserIdAndExpiryDateTimeIsAfter(userId: UUID, now: LocalDateTime): Mono<ConfirmationCode>
}