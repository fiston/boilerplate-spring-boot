package rw.viden.boilerplate.repository

import org.springframework.data.r2dbc.repository.R2dbcRepository
import org.springframework.stereotype.Repository
import reactor.core.publisher.Mono
import rw.viden.boilerplate.models.databases.Users
import rw.viden.boilerplate.utils.UserStatus
import java.util.*

@Repository
interface UsersRepository :R2dbcRepository<Users, UUID>{

    fun findByUsername(username: String): Mono<Users>

    fun existsByUsername(username: String): Mono<Boolean>

    fun findByIdAndStatus(id: UUID, status: UserStatus): Mono<Users>

    fun existsByIdAndStatus(id: UUID, status: UserStatus): Mono<Boolean>

}