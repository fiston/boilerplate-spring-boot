package rw.viden.boilerplate.repository

import org.springframework.data.r2dbc.repository.R2dbcRepository
import reactor.core.publisher.Mono
import rw.viden.boilerplate.models.databases.CustomerProfile
import java.util.*

interface CustomerProfileRepository:R2dbcRepository<CustomerProfile, UUID> {
    fun findByUserId(userId: UUID): Mono<CustomerProfile>
    fun existsByEmail(email: String): Mono<Boolean>
    fun existsByPhoneNumber(phoneNumber: String): Mono<Boolean>
    fun existsByPhoneNumberOrEmail(phoneNumber: String, email: String): Mono<Boolean>
}