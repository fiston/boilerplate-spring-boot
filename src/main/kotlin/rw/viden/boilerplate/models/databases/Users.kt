package rw.viden.boilerplate.models.databases


import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.Id
import org.springframework.data.annotation.LastModifiedDate
import org.springframework.data.annotation.Version
import org.springframework.data.domain.Persistable
import org.springframework.data.relational.core.mapping.Table
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.UserDetails
import rw.viden.boilerplate.utils.UserRoles
import rw.viden.boilerplate.utils.UserStatus
import java.util.*
import java.util.stream.Collectors

@Table(name = "users")
data class Users(
    @Id
    @get:JvmName(name = "id_")
    var id: UUID? = UUID.randomUUID(),

    @get:JvmName("_getPassword")
    var password: String,

    @get:JvmName("_getUsername")
    var username: String,

    var role: UserRoles = UserRoles.USER,

    ) : UserDetails, Persistable<UUID> {

    @org.springframework.data.annotation.Transient
    var isNewUser: Boolean = false

    @Version
    @Transient
    var version: Long = 0

    var status: UserStatus = UserStatus.PENDING_APPROVAL

    @CreatedDate
    var createdAt: Date = Date()

    @LastModifiedDate
    var updatedAt: Date = Date()

    var accountNonExpired: Boolean = true

    var accountNonLocked = true

    var credentialsNonExpired = true

    var enabled = false

    var isResettingPassword = false

    override fun getAuthorities(): MutableCollection<out GrantedAuthority> {
        val authorities: List<GrantedAuthority> = listOf(SimpleGrantedAuthority(role.name))
        return authorities.stream().map { authority -> SimpleGrantedAuthority(authority.authority) }
            .collect(Collectors.toList())
    }

    override fun getPassword(): String {
        return this.password
    }

    override fun getUsername(): String {
        return this.username
    }

    override fun isAccountNonExpired(): Boolean {
        return this.accountNonExpired
    }

    override fun isAccountNonLocked(): Boolean {
        return this.accountNonLocked
    }

    override fun isCredentialsNonExpired(): Boolean {
        return this.credentialsNonExpired
    }

    override fun isEnabled(): Boolean {
        return this.enabled
    }

    override fun getId(): UUID? {
        return id ?: UUID.randomUUID()
    }

    override fun isNew(): Boolean {
        return id == null || isNewUser
    }

    fun setAsNew(): Users {
        this.isNewUser = true
        return this
    }
}
