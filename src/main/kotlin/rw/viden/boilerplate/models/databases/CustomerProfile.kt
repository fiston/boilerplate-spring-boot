package rw.viden.boilerplate.models.databases

import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.Id
import org.springframework.data.annotation.LastModifiedDate
import org.springframework.data.annotation.Transient
import org.springframework.data.domain.Persistable
import org.springframework.data.relational.core.mapping.Column
import org.springframework.data.relational.core.mapping.Table
import rw.viden.boilerplate.utils.LegalDocumentType
import java.io.Serializable
import java.time.LocalDate
import java.time.LocalDateTime
import java.util.*

@Table("user_profile")
data class CustomerProfile(
    @Id
    @get:JvmName(name = "id_")
    var id: UUID? = null,
    @Column var firstName: String,
    @Column var lastName: String,
    @Column var phoneNumber: String,
    @Column var email: String,
    @Column var userId: UUID,

    @Column var legalDocument: String? = null,

    @Column var legalDocumentType: LegalDocumentType? = null,

    @Column var currentAddress: String? = null,

    @Column var permanentAddress: String? = null,

    @Column var contactNumber: String? = null,

    @Column var dateOfBirth: LocalDate? = null,

    @Column var kycComplete: Boolean,

    @Column var job: String? = null,

    @Column var employer: String? = null,

    @CreatedDate @Column var createdDate: LocalDateTime = LocalDateTime.now(),

    @LastModifiedDate @Column var lastUpdatedDateDate: LocalDateTime? = null
) : Serializable, Persistable<UUID> {

    @Transient
    var isNewUserProfile: Boolean = false

    override fun getId(): UUID? {
        return id ?: UUID.randomUUID()
    }

    override fun isNew(): Boolean {
        return id != null || isNewUserProfile
    }

    fun setAsNew(): CustomerProfile {
        this.isNewUserProfile = true
        return this
    }

    fun setIsKycComplete(): CustomerProfile {
        this.kycComplete = listOf(
            this.legalDocument,
            this.legalDocumentType,
            this.currentAddress,
            this.permanentAddress,
            this.contactNumber,
            this.dateOfBirth,
            this.job,
            this.employer
        ).all { it != null }
        return this
    }
}