package rw.viden.boilerplate.models.databases

import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.Id
import org.springframework.data.annotation.LastModifiedDate
import org.springframework.data.annotation.Transient
import org.springframework.data.domain.Persistable
import org.springframework.data.relational.core.mapping.Column
import org.springframework.data.relational.core.mapping.Table
import java.io.Serializable
import java.time.LocalDateTime
import java.util.*

@Table(name = "confirmation_code")
data class ConfirmationCode(

    @Id
    @Column
    @get:JvmName("getId_")
    var id: UUID?=null,
    var userId:UUID,
    var emailPhoneNumber: String,
    var token: String,
    var expiryDateTime: LocalDateTime,
    @CreatedDate
    @Column
    var createdDate: LocalDateTime=LocalDateTime.now(),

    @LastModifiedDate
    @Column
    var lastUpdatedDate: LocalDateTime=LocalDateTime.now(),

    ) : Serializable, Persistable<UUID> {

    @Transient
    private var isNewEmailConfirmation: Boolean = false

    override fun getId(): UUID? {
        return id ?: UUID.randomUUID()
    }

    override fun isNew(): Boolean {
        return id == null || isNewEmailConfirmation
    }
    fun setAsNew(): ConfirmationCode {
        this.id=UUID.randomUUID()
        this.isNewEmailConfirmation = true
        return this
    }
}