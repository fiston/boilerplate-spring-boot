package rw.viden.boilerplate.models.requests

import jakarta.validation.constraints.Email
import jakarta.validation.constraints.NotBlank
import jakarta.validation.constraints.Pattern
import org.springframework.format.annotation.DateTimeFormat
import rw.viden.boilerplate.utils.PHONE_REGEX
import java.io.Serializable
import java.time.LocalDate

data class SignupRequest(
    @get:NotBlank(message = "FIRSTNAME_CANNOT_BE_BLANK") var firstName: String,

    @get:NotBlank(message = "LASTNAME_CANNOT_BE_BLANK") var lastName: String,

    @get:NotBlank(message = "PHONE_NUMBER_CANNOT_BE_BLANK")
    @get:Pattern(regexp = PHONE_REGEX, message = "PHONE_NUMBER_NOT_VALID")
    var phoneNumber: String,

    @get:Email(message = "EMAIL_NOT_VALID") var email: String,

    @get:NotBlank(message = "USER_NAME_CANNOT_BE_BLANK") var username: String,

    @get:NotBlank(message = "PASSWORD_CANNOT_BE_BLANK") var password: String,

    @get:NotBlank(message = "LEGAL_DOCUMENT_CANNOT_BE_BLANK") var legalDocument: String,

    @get:NotBlank(message = "LEGAL_DOCUMENT_TYPE_CANNOT_BE_BLANK") var legalDocumentType: String,

    @get:NotBlank(message = "CURRENT_ADDRESS_CANNOT_BE_BLANK") var currentAddress: String,

    @get:NotBlank(message = "PERMANENT_ADDRESS_CANNOT_BE_BLANK") var permanentAddress: String,

    @get:NotBlank(message = "CONTACT_NUMBER_CANNOT_BE_BLANK") var contactNumber: String,

    @get:DateTimeFormat(iso = DateTimeFormat.ISO.DATE) var dateOfBirth: LocalDate,

    var kycComplete: Boolean = false,

    @get:NotBlank(message = "JOB_CANNOT_BE_BLANK") var job: String,

    @get:NotBlank(message = "EMPLOYER_CANNOT_BE_BLANK") var employer: String

) : Serializable
