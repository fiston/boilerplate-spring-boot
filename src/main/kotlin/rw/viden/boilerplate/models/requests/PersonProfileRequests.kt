package rw.viden.boilerplate.models.requests

import jakarta.validation.constraints.Email
import jakarta.validation.constraints.NotBlank
import jakarta.validation.constraints.NotNull
import jakarta.validation.constraints.Pattern
import rw.viden.boilerplate.utils.LegalDocumentType
import rw.viden.boilerplate.utils.PHONE_REGEX
import rw.viden.boilerplate.utils.UserRoles
import rw.viden.boilerplate.utils.ValidEnum

import java.io.Serializable

data class PersonProfileRequests(
    @get:NotBlank(message = "FIRSTNAME_CANNOT_BE_BLANK") var firstName: String,

    @get:NotBlank(message = "LASTNAME_CANNOT_BE_BLANK") var lastName: String,

    @get:NotBlank(message = "PHONE_NUMBER_CANNOT_BE_BLANK")
    @get:Pattern(regexp = PHONE_REGEX, message = "PHONE_NUMBER_NOT_VALID")
    var phoneNumber: String,

    @get:Email(message = "EMAIL_NOT_VALID") var email: String,

    @get:NotBlank(message = "USER_NAME_CANNOT_BE_BLANK") var username: String,

    @get:NotBlank(message = "PASSWORD_CANNOT_BE_BLANK") var password: String,

    @get:NotBlank(message = "ACCOUNT_NUMBER_CANNOT_BE_BLANK") val accountNumber: String,

    @get:NotBlank(message = "RSSB_REGISTRATION_NUMBER_CANNOT_BE_BLANK") val rssbRegistrationNumber: String,

    @get:NotBlank(message = "BUSINESS_NAME_CANNOT_BE_BLANK") val businessName: String,

    @get:NotBlank(message = "CORE_BANKING_ID_NUMBER_CANNOT_BE_BLANK") val coreBankingIdNumber: String

) : Serializable

class UsersProfileRequest(

    @get:NotBlank(message = "FIRSTNAME_CANNOT_BE_BLANK") var firstName: String,

    @get:NotBlank(message = "LASTNAME_CANNOT_BE_BLANK") var lastName: String,

    @get:NotBlank(message = "PHONE_NUMBER_CANNOT_BE_BLANK")
    @get:Pattern(regexp = PHONE_REGEX, message = "PHONE_NUMBER_NOT_VALID")
    var phoneNumber: String,

    @get:Email(message = "EMAIL_NOT_VALID") var email: String,

    @get:NotBlank(message = "USER_NAME_CANNOT_BE_BLANK") var username: String,
    @field:ValidEnum(
        message = "INVALID_TYPE",
        enumClass = UserRoles::class
    ) val role: UserRoles,

    @get:NotBlank(message = "LEGAL_DOCUMENT_CANNOT_BE_BLANK")var legalDocument: String,

    @field:ValidEnum(
        message = "INVALID_TYPE",
        enumClass = LegalDocumentType::class
    ) var legalDocumentType: LegalDocumentType,

    @get:NotBlank(message = "CURRENT_ADDRESS_CANNOT_BE_BLANK") var currentAddress: String,

    @get:NotBlank(message = "PERMANENT_CANNOT_BE_BLANK") var permanentAddress: String,

    @get:NotBlank(message = "CONTACT_NUMBER_CANNOT_BE_BLANK") var contactNumber: String,

    @get:NotNull(message = "DATE_OF_BIRTH_CANNOT_BE_BLANK") var dateOfBirth: String,

    @get:NotBlank(message = "JOB_CANNOT_BE_BLANK") var job: String,

    @get:NotBlank(message = "EMPLOYER_CANNOT_BE_BLANK") var employer: String,

    )

class SelfOnboardingRequest(

    @get:NotBlank(message = "FIRSTNAME_CANNOT_BE_BLANK") var firstName: String,

    @get:NotBlank(message = "LASTNAME_CANNOT_BE_BLANK") var lastName: String,

    @get:NotBlank(message = "PHONE_NUMBER_CANNOT_BE_BLANK")
    @get:Pattern(regexp = PHONE_REGEX, message = "PHONE_NUMBER_NOT_VALID")
    var phoneNumber: String,

    @get:Email(message = "EMAIL_NOT_VALID") var email: String,

    @get:NotBlank(message = "USER_NAME_CANNOT_BE_BLANK") var username: String,

    @get:NotBlank(message = "PASSWORD_CANNOT_BE_BLANK") var password: String,

    @field:ValidEnum(
        message = "INVALID_TYPE",
        enumClass = UserRoles::class,
        exclude = ["SECONDARY_ACCOUNT_OWNER"]
    ) val role: UserRoles

)


