package rw.viden.boilerplate.models.requests

import com.fasterxml.jackson.annotation.JsonProperty
import jakarta.validation.constraints.NotBlank
import java.io.Serializable


class AuthenticationRequest(
    @get:NotBlank(message = "USERNAME_CANNOT_BE_BLANK")
    @JsonProperty("username") val username: String,
    @get:NotBlank(message = "PASSWORD_CANNOT_BE_BLANK")
    @JsonProperty("password") val password: String,
) : Serializable {
    override fun toString(): String {
        return "AuthenticationRequest(username='$username', password='$password')"
    }
}


data class RefreshTokenRequest(
    @NotBlank @JsonProperty("refreshToken") val refreshToken: String
) : Serializable

data class LogoutRequest(
    @NotBlank @JsonProperty("accessToken") val accessToken: String,
    @NotBlank @JsonProperty("refreshToken") val refreshToken: String,
) : Serializable {

}

class VerifyPasswordRequest(
    @NotBlank @JsonProperty("username") val username: String,
    @NotBlank @JsonProperty("password") val password: String,
    @NotBlank @JsonProperty("accessToken") val accessToken: String,
) : Serializable {

    override fun toString(): String {
        return "VerifyPasswordRequest(" +
                "username='$username', " +
                "password='$password', " +
                "accessToken='$accessToken'" +
                ")"
    }
}

data class SelfPasswordResetRequest(
    val oldPassword: String,
    val newPassword: String
)

data class PasswordResetRequest(
    @get:NotBlank(message = "USERNAME_CANNOT_BE_BLANK") var username: String,
    @get:NotBlank(message = "PASSWORD_CANNOT_BE_BLANK") var newPassword: String
) : Serializable