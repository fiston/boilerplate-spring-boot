package rw.viden.boilerplate.models.responses

import com.fasterxml.jackson.annotation.JsonProperty
import jakarta.validation.constraints.NotBlank
import java.util.*

class AuthenticationResponse(

    @NotBlank @JsonProperty("token") val token: String,
    @NotBlank @JsonProperty("refreshToken") val refreshToken: String,
    @NotBlank @JsonProperty("isResettingPassword") val isResettingPassword: Boolean,
    @NotBlank @JsonProperty("userId") val userId: UUID
)