package rw.viden.boilerplate.models.responses

import java.io.Serializable

class HttpDefaultResponse(
    val status: String,
    val message: String,
    val timestamps: String
):Serializable{
    override fun toString(): String {
        return "{" +
                "status:'$status', " +
                "message:'$message', " +
                "timestamps:'$timestamps'" +
                "}"
    }
}