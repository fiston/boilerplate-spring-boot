package rw.viden.boilerplate.models.responses

import rw.viden.boilerplate.utils.UserStatus


class RegistrationResponse (
     val message:String,
     val status: UserStatus,
     val id: String?
)