package rw.viden.boilerplate.configurations

import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component


@Component
class ApplicationProperties {
    @Value("\${spring.mail.username}")
    val mailUsername: String = ""
}
