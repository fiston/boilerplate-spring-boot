package rw.viden.boilerplate.exceptions


import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.databind.ObjectMapper
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.context.annotation.Configuration
import org.springframework.core.Ordered.HIGHEST_PRECEDENCE
import org.springframework.core.annotation.Order
import org.springframework.core.io.buffer.DataBuffer
import org.springframework.http.MediaType
import org.springframework.web.server.ServerWebExchange
import org.springframework.web.server.WebExceptionHandler
import reactor.core.publisher.Mono
import rw.viden.boilerplate.utils.toHttpStatus
import java.util.*


@Configuration
@Order(HIGHEST_PRECEDENCE)
class GlobalExceptionHandler(private val objectMapper: ObjectMapper) : WebExceptionHandler {
    private val logger: Logger = LoggerFactory.getLogger(this::class.java)

    override fun handle(serverWebExchange: ServerWebExchange, throwable: Throwable): Mono<Void> {

        val bufferFactory = serverWebExchange.response.bufferFactory()

        throwable.toHttpStatus().also { serverWebExchange.response.statusCode = it }

        logger.info("<<<<<<<<<<<<<< Handling: ${serverWebExchange.response.statusCode} >>>>>>>>>>>>>>>>>>>>")

        val dataBuffer: DataBuffer = try {
            throwable.printStackTrace()
            bufferFactory.wrap(
                objectMapper.writeValueAsBytes(
                    HttpError(
                        status = serverWebExchange.response.statusCode!!.value(),
                        message = throwable.message.orEmpty(),
                        error = serverWebExchange.response.statusCode.toString(),
                        timestamps = Date().toString()
                    )
                )
            )
        } catch (e: JsonProcessingException) {
            bufferFactory.wrap("This should never be displayed, if it does, BADA-BOOM".toByteArray())
        }
        serverWebExchange.response.headers.contentType = MediaType.APPLICATION_JSON
        return serverWebExchange.response.writeWith(Mono.just(dataBuffer))
    }

    inner class HttpError internal constructor(
        val status: Int,
        val error: String,
        val message: String,
        val timestamps: String
    )


}