package rw.viden.boilerplate

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class BoilerPlateApplication


fun main(args: Array<String>) {

    runApplication<BoilerPlateApplication>(*args)
}
