package rw.viden.boilerplate.security

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.web.ServerProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.core.annotation.Order
import org.springframework.http.HttpStatus
import org.springframework.security.config.annotation.method.configuration.EnableReactiveMethodSecurity
import org.springframework.security.config.annotation.web.builders.WebSecurity
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity
import org.springframework.security.config.web.server.ServerHttpSecurity
import org.springframework.security.config.web.server.ServerHttpSecurity.AuthorizeExchangeSpec
import org.springframework.security.core.userdetails.ReactiveUserDetailsService
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.web.server.SecurityWebFilterChain
import org.springframework.security.web.server.authorization.HttpStatusServerAccessDeniedHandler
import org.springframework.web.cors.CorsConfiguration
import org.springframework.web.cors.reactive.CorsConfigurationSource
import org.springframework.web.cors.reactive.UrlBasedCorsConfigurationSource
import rw.viden.boilerplate.services.UsersService


@Configuration
@EnableWebFluxSecurity
@EnableReactiveMethodSecurity
@Order(-2)
class WebSecurityConfiguration {

    @Autowired
    private lateinit var authenticationManager: AuthenticationManager

    @Autowired
    private lateinit var securityContextRepository: SecurityContextRepository

    @Autowired
    private lateinit var userRepository: UsersService

    @Autowired
    private lateinit var serverProperties: ServerProperties

    private val logger: Logger = LoggerFactory.getLogger(WebSecurityConfiguration::class.java)


    @Bean
    fun springSecurityFilterChain(http: ServerHttpSecurity): SecurityWebFilterChain? {

        http // ...
            .securityContextRepository(securityContextRepository)
            .authenticationManager(authenticationManager)
            .authorizeExchange { exchanges: AuthorizeExchangeSpec ->
                exchanges // any URL that starts with /admin/ requires the role "ROLE_ADMIN"
                    .pathMatchers("/boilerplate/admin/**").hasRole("ROLE_ADMIN")
                    .pathMatchers("/boilerplate/twese/**").hasAnyRole("ROLE_ADMIN", "ROLE_USER")
                    .pathMatchers("/boilerplate/client/**").hasRole("ROLE_USER")
                    .pathMatchers(
                        "/",
                        "/create-user",
                        "/signup",
                        "/login",
                        "/v3/api-docs/**",
                        "/swagger-ui.html",
                        "/webjars/**",
                        "/messaging/v3/api-docs",
                        "/all-users",
                        "/actuator/**",
                        "/actuator/info",
                        "/actuator/health",
                        "/activate-user/**",
                        "/re-send-activation-code/**"
                    )
                    .permitAll()
                    .anyExchange().authenticated()

            }.exceptionHandling(
                ({ exceptionHandling ->
                    exceptionHandling // customize how to request for authentication
                        .accessDeniedHandler(HttpStatusServerAccessDeniedHandler(HttpStatus.FORBIDDEN))
                })

            ).csrf { csrf -> csrf.disable() }
        return http.build()
    }

    @Bean
    fun bCryptPasswordEncoder(): BCryptPasswordEncoder {
        return BCryptPasswordEncoder()
    }

    @Bean
    fun userDetailsService(): ReactiveUserDetailsService? {
        return ReactiveUserDetailsService { username: String ->
            userRepository.findByUsername(username).map { it as UserDetails }
        }
    }

    @Bean
    fun corsConfigurationSource(): CorsConfigurationSource {
        val configuration = CorsConfiguration()
        configuration.addAllowedOrigin("*")
        configuration.addAllowedHeader("*")
        configuration.addAllowedMethod("*")
        configuration.maxAge = 3600L
        val source = UrlBasedCorsConfigurationSource()
        source.registerCorsConfiguration("/**", configuration)
        return source
    }

    @Bean
    fun corsFilters(): CorsFilter {
        return CorsFilter(corsConfigurationSource())
    }
    @Throws(Exception::class)
    protected fun configure(web: WebSecurity) {
        val basePath = serverProperties.servlet.contextPath
        web.ignoring().requestMatchers("$basePath/**")
    }
}