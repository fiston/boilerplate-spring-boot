package rw.viden.boilerplate.security

import io.jsonwebtoken.Claims
import org.slf4j.Logger
import org.springframework.security.authentication.ReactiveAuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.stereotype.Component
import reactor.core.publisher.Mono
import java.util.stream.Collectors


@Component
class AuthenticationManager(private val jwtUtil: JwtUtils) : ReactiveAuthenticationManager {

    val logger:Logger = org.slf4j.LoggerFactory.getLogger(AuthenticationManager::class.java)

    override fun authenticate(authentication: Authentication): Mono<Authentication> {
        val authToken: String = authentication.credentials.toString()
        logger.info("Authenticating with authToken: $authToken")
        val username: String? = try {
            jwtUtil.getUserNameFromToken(authToken)
        } catch (e: Exception) {
            e.printStackTrace()
            null
        }

        logger.info("<> ${username != null && jwtUtil.validateToken(authToken)}")
        return if (username != null && jwtUtil.validateToken(authToken)) {
            logger.info("Authenticating with username: $username \n")
            val claims: Claims = jwtUtil.getAllClaimsFromToken(authToken)
            val rolesMap: List<String> = listOf<String>(claims.get(
                    "role",
                    String::class.java
                ))

            val roles: MutableList<String> = ArrayList()
            for (roleMap in rolesMap) {
                roles.add(roleMap)
            }
            val auth = UsernamePasswordAuthenticationToken(
                username,
                null,
                roles.stream().map { authority: String ->
                    SimpleGrantedAuthority(
                        authority
                    )
                }.collect(Collectors.toList())
            )
            logger.info("\n\n ======= Auth: $auth \n")
            Mono.just(auth)
        } else {
            logger.info("Error ${ jwtUtil.validateToken(authToken)} ====== $username")
            Mono.empty()
        }
    }



}

