package rw.viden.boilerplate.security

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.http.server.reactive.ServerHttpRequest
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.core.context.SecurityContext
import org.springframework.security.core.context.SecurityContextImpl
import org.springframework.security.web.server.context.ServerSecurityContextRepository
import org.springframework.stereotype.Component
import org.springframework.web.server.ServerWebExchange
import reactor.core.publisher.Mono
import rw.viden.boilerplate.exceptions.NotFoundException

@Component
class SecurityContextRepository : ServerSecurityContextRepository {

    @Autowired
    private lateinit var authenticationManager: AuthenticationManager

    private val logger: Logger = LoggerFactory.getLogger(SecurityContextRepository::class.java)


    override fun save(swe: ServerWebExchange?, sc: SecurityContext): Mono<Void> {
        throw UnsupportedOperationException("Not supported yet.")
    }

    override fun load(swe: ServerWebExchange): Mono<SecurityContext>? {
        val request: ServerHttpRequest = swe.request
        val authHeader: String? = request.headers[HttpHeaders.AUTHORIZATION]?.firstOrNull()


        return if (authHeader != null && !isOpenUrl(request)) {
            if (authHeader.isNotEmpty() && authHeader.startsWith("Bearer ")) {

                val authToken = authHeader.substring(7)

                val auth: Authentication = UsernamePasswordAuthenticationToken(authToken, authToken)
                logger.info("Authenticating with authToken: $auth")
                authenticationManager.authenticate(auth)
                    .switchIfEmpty(Mono.error(NotFoundException("User not found")))
                    .map { authentication: Authentication? -> SecurityContextImpl(authentication) }

            } else {
                logger.info("${authHeader.isNotEmpty() && authHeader.startsWith("Bearer ")}")
                Mono.empty()
            }
        } else {
            Mono.empty()
        }
    }


    private fun isOpenUrl(request: ServerHttpRequest): Boolean {
        val path = request.path.toString()
        return (path.contains("refresh_token"))

    }
}