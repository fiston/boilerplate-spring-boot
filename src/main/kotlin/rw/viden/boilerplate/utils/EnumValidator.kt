package rw.viden.boilerplate.utils

import jakarta.validation.ConstraintValidator
import jakarta.validation.ConstraintValidatorContext

class EnumValidator : ConstraintValidator<ValidEnum, Enum<*>> {
    private lateinit var validValues: Set<String>
    private lateinit var excludedRole: Array<String>

    override fun initialize(constraintAnnotation: ValidEnum) {
        validValues = constraintAnnotation.enumClass.java.enumConstants.map { it.toString() }.toSet()
        excludedRole = constraintAnnotation.exclude
    }

    override fun isValid(
        value: Enum<*>,
        context: ConstraintValidatorContext?,
    ): Boolean {
        return validValues.contains(value.name) && !excludedRole.contains(value.name)
    }
}
