package rw.viden.boilerplate.utils


import org.springframework.core.convert.converter.Converter
import org.springframework.data.convert.ReadingConverter
import org.springframework.data.convert.WritingConverter
import java.time.LocalDateTime
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import java.util.*

@WritingConverter
class LocalDateTimeToDateConverter : Converter<LocalDateTime, Date> {
    override fun convert(source: LocalDateTime): Date {
        val instant = source.atZone(ZoneId.systemDefault()).toInstant()
        return Date.from(instant)
    }
}

@ReadingConverter
class DateToLocalDateTimeConverter : Converter<Date, LocalDateTime> {
    override fun convert(source: Date): LocalDateTime {
        val instant = source.toInstant()
        return LocalDateTime.ofInstant(instant, ZoneId.systemDefault())
    }
}


@ReadingConverter
class LocalDateTimeToStringConverter(pattern: String) :
    Converter<LocalDateTime, String> {
    private val formatter: DateTimeFormatter

    init {
        formatter = DateTimeFormatter.ofPattern(pattern)
    }

    override fun convert(source: LocalDateTime): String {
        return source.format(formatter)
    }
}