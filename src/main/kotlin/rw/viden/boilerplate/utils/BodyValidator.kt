package rw.viden.boilerplate.utils

import jakarta.validation.ConstraintViolation
import jakarta.validation.Validator
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.ServerResponse
import reactor.core.publisher.Mono
import rw.viden.boilerplate.exceptions.BadRequestException
import java.util.stream.Collectors

@Component
class BodyValidator(private val validator: Validator) {

    fun <BODY> withValidBody(
        block: (Mono<BODY>) -> Mono<ServerResponse>,
        request: ServerRequest, bodyClass: Class<BODY>
    ): Mono<ServerResponse> {

        if (request.headers().contentLength().orElse(0) <= 0)
            throw BadRequestException("BAD_REQUEST")

        return request
            .bodyToMono(bodyClass)
            .flatMap { body ->

                if (body == null) {
                    error(BadRequestException("BAD_REQUEST"))
                }

                val violations = validator.validate(body)

                if (violations.isEmpty())
                    block.invoke(Mono.just(body))
                else {


                    val error: String = violations.stream().map { cv: ConstraintViolation<*>? ->
                        if (cv == null) "null" else cv.message
                    }.collect(Collectors.joining(", "))

                    Mono.error(BadRequestException(error))
                }

            }

    }

}