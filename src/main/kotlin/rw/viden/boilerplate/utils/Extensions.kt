package rw.viden.boilerplate.utils

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import io.r2dbc.postgresql.codec.Json
import org.springframework.dao.DataIntegrityViolationException
import org.springframework.dao.EmptyResultDataAccessException
import org.springframework.http.HttpStatus
import org.springframework.http.converter.HttpMessageNotReadableException
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException
import rw.viden.boilerplate.exceptions.*
import java.text.SimpleDateFormat
import java.util.*
import com.fasterxml.jackson.module.kotlin.registerKotlinModule

fun Throwable.handleAndThrowException() {

    when {
        localizedMessage.contains("InvalidFormatException") -> throw BadRequestException("INVALID_FORMAT" + this.message)
        localizedMessage.contains("ValueInstantiationException") -> throw BadRequestException("PARAMETER_MISSING")
        else -> throw this
    }
}

fun Throwable.toHttpStatus(): HttpStatus {
    return when (this) {

        is ParameterNeededException -> {
            HttpStatus.BAD_REQUEST
        }

        is HttpMessageNotReadableException -> {
            HttpStatus.BAD_REQUEST
        }

        is IllegalAccessException -> {
            HttpStatus.BAD_REQUEST
        }

        is IllegalArgumentException -> {
            HttpStatus.BAD_REQUEST
        }

        is DataIntegrityViolationException -> {
            HttpStatus.FORBIDDEN
        }

        is UnauthorizedException -> {
            HttpStatus.UNAUTHORIZED

        }

        is NotFoundException -> {
            HttpStatus.NOT_FOUND

        }

        is NoSuchElementException -> {
            HttpStatus.NOT_FOUND

        }

        is MethodArgumentTypeMismatchException -> {
            HttpStatus.NOT_FOUND

        }

        is EmptyResultDataAccessException -> {
            HttpStatus.NOT_FOUND

        }

        is InternalServerErrorException -> {
            HttpStatus.INTERNAL_SERVER_ERROR

        }

        is ForbiddenException -> {
            HttpStatus.FORBIDDEN

        }

        is InvalidJwtTokenException -> {
            HttpStatus.UNAUTHORIZED
        }

        is BadRequestException -> {
            HttpStatus.BAD_REQUEST
        }

        is DuplicateViolation -> {
            HttpStatus.CONFLICT
        }

        is AccountStatusException -> {
            HttpStatus.UNAUTHORIZED
        }

        is TokenRevokedException -> {
            HttpStatus.UNAUTHORIZED
        }

        else -> {
            HttpStatus.INTERNAL_SERVER_ERROR
        }
    }
}
fun String.toDate(): Date {
    val formatter = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
    formatter.timeZone = TimeZone.getTimeZone("UTC")
    return formatter.parse(this)
}

fun String?.isValidRwandanPhoneNumber(): Boolean = this.isNullOrBlank().not() && this!!.matches(Regex(PHONE_REGEX))

fun String.toRwandan10DigitPhone(): String {

    val len = this.length

    return if (len == 12 && this.matches(Regex("^250\\d{9}$"))) this.takeLast(10)
    else if (len == 10 && this.matches(Regex("^0\\d{9}$"))) this
    else if (len == 9 && this.matches(Regex("^\\d{9}$"))) "0$this"
    else ""
}

fun String.toRwandan12DigitPhone(): String {

    val len = this.length

    return if (len == 12 && this.matches(Regex("^250\\d{9}$"))) this
    else if (len == 10 && this.matches(Regex("^0\\d{9}$"))) "25$this"
    else if (len == 9 && this.matches(Regex("^\\d{9}$"))) "250$this"
    else ""
}

fun Any?.transformToJsonNode(): JsonNode {
    val objectMapper: ObjectMapper =
        ObjectMapper().registerKotlinModule()
            .disable(SerializationFeature.FAIL_ON_EMPTY_BEANS)
            .disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES).registerModule(JavaTimeModule())

    return objectMapper.valueToTree(this)
}

fun Json.toBeautifulString(): String {
    val objectMapper =
        ObjectMapper().registerKotlinModule().disable(SerializationFeature.FAIL_ON_EMPTY_BEANS)
            .disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES)
    return objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(this)
}

fun Any?.toPrettyJsonString(): String {
    return this.transformToJsonNode().toPrettyString()
}