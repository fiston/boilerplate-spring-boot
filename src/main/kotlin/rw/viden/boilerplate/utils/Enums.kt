package rw.viden.boilerplate.utils


enum class UserStatus{
    PENDING_APPROVAL,
    ACTIVE,
    REJECTED,
    DELETED
}

enum class UserType{
    CUSTOMER,
    EMPLOYEE,
}

enum class Priorities {
    LOW,
    MEDIUM,
    HIGH
}

enum class UserRoles {
    ADMIN,
    USER
}

enum class LegalDocumentType {
    NATIONAL_ID,
    PASSPORT,
    DRIVING_LICENSE
}