
CREATE TABLE IF NOT EXISTS customer_additional_information
(
    id                                 UUID NOT NULL,
    loan_id                            VARCHAR(255),
    requested_additional_info          VARCHAR(255),
    request_date                       TIMESTAMP WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    response_date                      TIMESTAMP WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    requested_additional_info_response VARCHAR(255),
    request_id                         VARCHAR(255),
    processing_stage                   VARCHAR(255),
    info_requested_by                  VARCHAR(255),
    CONSTRAINT pk_customeradditionalinformation PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS customer_profiles
(
    id                       UUID    NOT NULL,
    first_name               VARCHAR(255),
    last_name                VARCHAR(255),
    email                    VARCHAR(255),
    phone_number             VARCHAR(255),
    user_id                  UUID,
    account_number           VARCHAR(255),
    rssb_registration_number VARCHAR(255),
    business_name            VARCHAR(255),
    core_banking_id_number   VARCHAR(255),
    created_by               VARCHAR(255),
    created_at               TIMESTAMP WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    is_new_profile           BOOLEAN NOT NULL,
    CONSTRAINT pk_customer_profiles PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS person
(
    id                     UUID NOT NULL,
    first_name             VARCHAR(255),
    last_name              VARCHAR(255),
    email                  VARCHAR(255),
    phone_number           VARCHAR(255),
    legal_document         VARCHAR(255),
    legal_document_type    VARCHAR(255),
    current_address        VARCHAR(255),
    permanent_address      VARCHAR(255),
    contact_number         VARCHAR(255),
    date_of_birth          date,
    kyc_complete           BOOLEAN,
    job                    VARCHAR(255),
    employer               VARCHAR(255),
    created_date           TIMESTAMP WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    last_updated_date_date TIMESTAMP WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    CONSTRAINT pk_person PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS user_profile
(
    id                     UUID NOT NULL,
    first_name             VARCHAR(255),
    last_name              VARCHAR(255),
    phone_number           VARCHAR(255),
    email                  VARCHAR(255),
    user_id                UUID,
    legal_document         VARCHAR(255),
    legal_document_type    VARCHAR(255),
    current_address        VARCHAR(255),
    permanent_address      VARCHAR(255),
    contact_number         VARCHAR(255),
    date_of_birth          date,
    kyc_complete           BOOLEAN,
    job                    VARCHAR(255),
    employer               VARCHAR(255),
    created_date           TIMESTAMP WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    last_updated_date_date TIMESTAMP WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    CONSTRAINT pk_user_profile PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS users
(
    id                      UUID PRIMARY KEY,
    password                VARCHAR(255) NOT NULL,
    username                VARCHAR(255) NOT NULL,
    role                    VARCHAR(255) NOT NULL,
    version                 BIGINT       NOT NULL,
    status                  VARCHAR(255) NOT NULL,
    user_type               VARCHAR(255) NOT NULL,
    created_at              TIMESTAMP WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    updated_at              TIMESTAMP WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    account_non_expired     BOOLEAN      NOT NULL,
    account_non_locked      BOOLEAN      NOT NULL,
    credentials_non_expired BOOLEAN      NOT NULL,
    enabled                 BOOLEAN      NOT NULL,
    is_resetting_password   BOOLEAN      NOT NULL
);
CREATE TABLE confirmation_code
(
    id                UUID PRIMARY KEY,
    user_id           UUID         NOT NULL,
    email             VARCHAR(255) NOT NULL,
    token             VARCHAR(255) NOT NULL,
    expiry_date_time  TIMESTAMP    NOT NULL,
    created_date      TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    last_updated_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

DO
$$
    BEGIN
        IF (SELECT COUNT(*) FROM users) = 0 THEN
            -- Insert data only if the table is empty
            INSERT INTO users (id, "password", username, "role", "version", status, created_at, account_non_expired,
                               account_non_locked, credentials_non_expired, enabled, user_type, is_resetting_password,
                               updated_at)
            VALUES ('de9d7eba-e6e1-487a-8486-c2967f663667',
                    '$2a$10$N4JH7gY0BjJOkhF5BzrBMOEr9CRV68Jd7fqaES/GimuQ1pu3KybuK', 'admin', 'ADMIN', 1,
                    '2023-12-06 13:30:26.447', NULL, true, true, true, true, 'EMPLOYEE', false,
                    '2023-12-06 13:30:26.447');

        END IF;
    END
$$