FROM gradle:latest

COPY . /app

WORKDIR /app

RUN gradle build --scan

EXPOSE 2236

ENTRYPOINT ["java", "-jar", "./build/libs/Boiler-Plate-0.0.1-SNAPSHOT.jar"]